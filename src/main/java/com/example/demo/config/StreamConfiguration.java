package com.example.demo.config;

import com.example.demo.stream.PaymentsStream;
import org.springframework.cloud.stream.annotation.EnableBinding;

@EnableBinding(PaymentsStream.class)
public class StreamConfiguration {
}
