package com.example.demo.model.binding;

public class ExceptionDto {

    private String message;
    private Boolean success;

    public ExceptionDto() {
    }

    public ExceptionDto(String message, Boolean success) {
        this.message = message;
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public ExceptionDto setMessage(String message) {
        this.message = message;
        return this;
    }

    public Boolean getSuccess() {
        return success;
    }

    public ExceptionDto setSuccess(Boolean success) {
        this.success = success;
        return this;
    }
}
