package com.example.demo.model.binding;

import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.*;
import java.io.Serializable;

public class DemoRequestDataBindingModel implements Serializable {

    @Size(min = 4, max = 20, message = "Full name length must be between 8 and 20 symbols.")
    @NotBlank(message = "Full name is required")
    private String fullName;
    @Size(min = 2, max = 20, message = "Company name length must be between 8 and 20 symbols.")
    @NotBlank(message = "Company name is required")
    private String companyName;
    @Email(message = "Invalid email")
    @NotBlank(message = "Email is required")
    private String email;
    @Pattern(regexp = "^[0-9]+$", message = "Enter valid number!")
    @NotBlank(message = "Phone number is required!")
    private String phoneNumber;

    public DemoRequestDataBindingModel() {
    }

    public DemoRequestDataBindingModel(String fullName, String companyName, String email, String phoneNumber) {
        this.fullName = fullName;
        this.companyName = companyName;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public DemoRequestDataBindingModel setFullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public String getCompanyName() {
        return companyName;
    }

    public DemoRequestDataBindingModel setCompanyName(String companyName) {
        this.companyName = companyName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public DemoRequestDataBindingModel setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public DemoRequestDataBindingModel setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }
}
