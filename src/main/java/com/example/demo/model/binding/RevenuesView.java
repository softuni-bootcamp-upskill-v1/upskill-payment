package com.example.demo.model.binding;

import java.math.BigDecimal;

public class RevenuesView {

    private String name;
    private BigDecimal money;

    public String getName() {
        return name;
    }

    public RevenuesView setName(String name) {
        this.name = name;
        return this;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public RevenuesView setMoney(BigDecimal money) {
        this.money = money;
        return this;
    }

    public RevenuesView() {
    }
}
