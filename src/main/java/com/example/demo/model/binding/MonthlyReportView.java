package com.example.demo.model.binding;

import java.math.BigDecimal;
import java.util.List;

public class MonthlyReportView {

    private List<String> months;
    private List<Integer> monthNumbers;
    private List<BigDecimal> revenues;

    public List<String> getMonths() {
        return months;
    }

    public MonthlyReportView setMonths(List<String> months) {
        this.months = months;
        return this;
    }

    public List<Integer> getMonthNumbers() {
        return monthNumbers;
    }

    public MonthlyReportView setMonthNumbers(List<Integer> monthNumbers) {
        this.monthNumbers = monthNumbers;
        return this;
    }

    public List<BigDecimal> getRevenues() {
        return revenues;
    }

    public MonthlyReportView setRevenues(List<BigDecimal> revenues) {
        this.revenues = revenues;
        return this;
    }

    public MonthlyReportView() {
    }
}
