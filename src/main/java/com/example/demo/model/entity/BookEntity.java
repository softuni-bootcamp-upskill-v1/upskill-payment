package com.example.demo.model.entity;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.math.BigDecimal;
import java.time.LocalDate;

@Document("bookings")
public class BookEntity {

    @MongoId(targetType = FieldType.OBJECT_ID)
    private String id;
    @Field(name = "date_of_booking")
    private LocalDate dateOfBooking;

    @Field(name = "id_of_booked_item")
    private String idOfBookedItem;

    @Field(name = "owner_of_booking")
    private String ownerOfBooking;

    public String getOwnerOfBooking() {
        return ownerOfBooking;
    }

    public BookEntity setOwnerOfBooking(String ownerOfBooking) {
        this.ownerOfBooking = ownerOfBooking;
        return this;
    }

    private Integer month;

    public String getIdOfBookedItem() {
        return idOfBookedItem;
    }

    public BookEntity setIdOfBookedItem(String idOfBookedItem) {
        this.idOfBookedItem = idOfBookedItem;
        return this;
    }

    private Integer year;

    public Integer getMonth() {
        return month;
    }

    public BookEntity setMonth(Integer month) {
        this.month = month;
        return this;
    }

    public Integer getYear() {
        return year;
    }

    public BookEntity setYear(Integer year) {
        this.year = year;
        return this;
    }

    private BigDecimal price;

    public BookEntity() {
    }

    public BookEntity(String id, LocalDate dateOfBooking, BigDecimal price) {
        this.id = id;
        this.dateOfBooking = dateOfBooking;
        this.price = price;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public BookEntity setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public String getId() {
        return id;
    }

    public BookEntity setId(String id) {
        this.id = id;
        return this;
    }

    public LocalDate getDateOfBooking() {
        return dateOfBooking;
    }

    public BookEntity setDateOfBooking(LocalDate dateOfBooking) {
        this.dateOfBooking = dateOfBooking;
        return this;
    }
}
