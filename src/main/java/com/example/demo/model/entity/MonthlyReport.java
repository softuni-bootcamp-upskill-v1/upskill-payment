package com.example.demo.model.entity;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.time.LocalDate;
import java.util.List;

@Document("monthly_report")
public class MonthlyReport {

    @MongoId(targetType = FieldType.OBJECT_ID)
    private String id;
    private LocalDate date;

    private Integer month;

    private Integer year;

    private List<BookEntity> bookings;

    public MonthlyReport() {
    }

    public MonthlyReport(String id, LocalDate date, List<BookEntity> bookings) {
        this.id = id;
        this.date = date;
        this.bookings = bookings;
    }

    public String getId() {
        return id;
    }

    public MonthlyReport setId(String id) {
        this.id = id;
        return this;
    }

    public LocalDate getDate() {
        return date;
    }

    public MonthlyReport setDate(LocalDate date) {
        this.date = date;
        return this;
    }

    public List<BookEntity> getBookings() {
        return bookings;
    }

    public MonthlyReport setBookings(List<BookEntity> bookings) {
        this.bookings = bookings;
        return this;
    }

    public Integer getMonth() {
        return month;
    }

    public MonthlyReport setMonth(Integer month) {
        this.month = month;
        return this;
    }

    public Integer getYear() {
        return year;
    }

    public MonthlyReport setYear(Integer year) {
        this.year = year;
        return this;
    }
}
