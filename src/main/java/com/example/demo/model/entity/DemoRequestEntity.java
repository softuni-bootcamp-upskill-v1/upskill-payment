package com.example.demo.model.entity;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Document("demo_request")
public class DemoRequestEntity {

    @MongoId(targetType = FieldType.OBJECT_ID)
    private String id;
    @Field(name = "full_name")
    @NotBlank
    private String fullName;
    @Field(name = "company_name")
    @NotBlank
    private String companyName;
    @Email
    @NotBlank
    private String email;
    @NotBlank
    @Field(name = "phone_number")
    private String phoneNumber;

    public DemoRequestEntity() {
    }

    public DemoRequestEntity(String id, String fullName, String companyName, String email, String phoneNumber) {
        this.id = id;
        this.fullName = fullName;
        this.companyName = companyName;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public String getId() {
        return id;
    }

    public DemoRequestEntity setId(String id) {
        this.id = id;
        return this;
    }

    public String getFullName() {
        return fullName;
    }

    public DemoRequestEntity setFullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public String getCompanyName() {
        return companyName;
    }

    public DemoRequestEntity setCompanyName(String companyName) {
        this.companyName = companyName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public DemoRequestEntity setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public DemoRequestEntity setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }
}
