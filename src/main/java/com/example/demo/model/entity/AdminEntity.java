package com.example.demo.model.entity;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.math.BigDecimal;
import java.util.List;

@Document("admin")
public class AdminEntity {

    @MongoId(targetType = FieldType.OBJECT_ID)
    private String id;

    @Field
    private List<CoachEntity> coaches;

    @Field
    private List<CourseEntity> courses;


}
