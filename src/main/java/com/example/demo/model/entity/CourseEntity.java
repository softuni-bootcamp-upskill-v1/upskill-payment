package com.example.demo.model.entity;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.math.BigDecimal;
import java.util.List;

@Document("courses")
public class CourseEntity {

    @MongoId(targetType = FieldType.OBJECT_ID)
    private String id;
    @Field(name = "course_id")
    private String courseId;
    @Field(name = "name")
    private String name;
    private BigDecimal coursePrice;
    private List<BookEntity> bookings;
    private Integer enrolled;

    public CourseEntity() {
    }

    public String getName() {
        return name;
    }

    public Integer getEnrolled() {
        return enrolled;
    }

    public CourseEntity setEnrolled(Integer enrolled) {
        this.enrolled = enrolled;
        return this;
    }

    public CourseEntity setName(String name) {
        this.name = name;
        return this;
    }

    public List<BookEntity> getBookings() {
        return bookings;
    }

    public CourseEntity setBookings(List<BookEntity> bookings) {
        this.bookings = bookings;
        return this;
    }

    public String getId() {
        return id;
    }

    public CourseEntity setId(String id) {
        this.id = id;
        return this;
    }

    public String getCourseId() {
        return courseId;
    }

    public CourseEntity setCourseId(String courseId) {
        this.courseId = courseId;
        return this;
    }

    public BigDecimal getCoursePrice() {
        return coursePrice;
    }

    public CourseEntity setCoursePrice(BigDecimal coursePrice) {
        this.coursePrice = coursePrice;
        return this;
    }
}
