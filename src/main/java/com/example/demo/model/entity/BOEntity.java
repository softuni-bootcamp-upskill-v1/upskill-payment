package com.example.demo.model.entity;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.lang.annotation.Documented;
import java.util.List;

@Document("business_owner")
public class BOEntity {

    @MongoId(targetType = FieldType.OBJECT_ID)
    private String id;
    @Field(name = "business_owner_id")
    private String businessOwnerId;
    private String email;
    private List<CourseEntity> courses;
    private List<CoachEntity> coaches;

    public BOEntity() {
    }

    public BOEntity(String id, String businessOwnerId, String email, List<CourseEntity> courses, List<CoachEntity> coaches) {
        this.id = id;
        this.businessOwnerId = businessOwnerId;
        this.email = email;
        this.courses = courses;
        this.coaches = coaches;
    }

    public String getId() {
        return id;
    }

    public BOEntity setId(String id) {
        this.id = id;
        return this;
    }

    public String getBusinessOwnerId() {
        return businessOwnerId;
    }

    public BOEntity setBusinessOwnerId(String businessOwnerId) {
        this.businessOwnerId = businessOwnerId;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public BOEntity setEmail(String email) {
        this.email = email;
        return this;
    }

    public List<CourseEntity> getCourses() {
        return courses;
    }

    public BOEntity setCourses(List<CourseEntity> courses) {
        this.courses = courses;
        return this;
    }

    public List<CoachEntity> getCoaches() {
        return coaches;
    }

    public BOEntity setCoaches(List<CoachEntity> coaches) {
        this.coaches = coaches;
        return this;
    }
}
