package com.example.demo.model.entity;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.math.BigDecimal;
import java.util.List;

@Document("coaches")
public class CoachEntity {

    @MongoId(targetType = FieldType.OBJECT_ID)
    private String id;
    @Field(name = "coach_id")
    private String coachId;
    private String name;
    private BigDecimal coachPrice;
    private List<BookEntity> bookings;
//    private Integer sessions;

    public CoachEntity() {
    }

    public CoachEntity(String id, String coachId, String name, BigDecimal coachPrice, List<BookEntity> bookings) {
        this.id = id;
        this.coachId = coachId;
        this.name = name;
        this.coachPrice = coachPrice;
        this.bookings = bookings;
    }

    public String getName() {
        return name;
    }

    public CoachEntity setName(String name) {
        this.name = name;
        return this;
    }

    public String getId() {
        return id;
    }

    public CoachEntity setId(String id) {
        this.id = id;
        return this;
    }

    public String getCoachId() {
        return coachId;
    }

    public CoachEntity setCoachId(String coachId) {
        this.coachId = coachId;
        return this;
    }

    public BigDecimal getCoachPrice() {
        return coachPrice;
    }

    public CoachEntity setCoachPrice(BigDecimal coachPrice) {
        this.coachPrice = coachPrice;
        return this;
    }

    public List<BookEntity> getBookings() {
        return bookings;
    }

    public CoachEntity setBookings(List<BookEntity> bookings) {
        this.bookings = bookings;
        return this;
    }
}
