package com.example.demo.model.view;

public class CoachNameSessionsView {

   private String name;
    private Integer sessions;

    public CoachNameSessionsView() {
    }

    public CoachNameSessionsView(String name, Integer sessions) {
        this.name = name;
        this.sessions = sessions;
    }

    public String getName() {
        return name;
    }

    public CoachNameSessionsView setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getSessions() {
        return sessions;
    }

    public CoachNameSessionsView setSessions(Integer sessions) {
        this.sessions = sessions;
        return this;
    }
}
