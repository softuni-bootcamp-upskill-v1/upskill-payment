package com.example.demo.model.view;

import java.math.BigDecimal;
import java.time.LocalDate;

public class InvoiceModelView {

    private String modelName;
    private LocalDate date;
    private BigDecimal price;

    public InvoiceModelView() {
    }

    public String getModelName() {
        return modelName;
    }

    public InvoiceModelView setModelName(String modelName) {
        this.modelName = modelName;
        return this;
    }

    public LocalDate getDate() {
        return date;
    }

    public InvoiceModelView setDate(LocalDate date) {
        this.date = date;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public InvoiceModelView setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }
}
