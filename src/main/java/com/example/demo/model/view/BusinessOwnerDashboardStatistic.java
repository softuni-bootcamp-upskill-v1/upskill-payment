package com.example.demo.model.view;

import java.util.List;

public class BusinessOwnerDashboardStatistic {

    private List<CourseNameEnrolledView> courses;
    private List<CoachNameSessionsView> coaches;


    public BusinessOwnerDashboardStatistic() {
    }

    public BusinessOwnerDashboardStatistic(List<CourseNameEnrolledView> courses, List<CoachNameSessionsView> coaches) {
        this.courses = courses;
        this.coaches = coaches;
    }

    public List<CourseNameEnrolledView> getCourses() {
        return courses;
    }

    public BusinessOwnerDashboardStatistic setCourses(List<CourseNameEnrolledView> courses) {
        this.courses = courses;
        return this;
    }

    public List<CoachNameSessionsView> getCoaches() {
        return coaches;
    }

    public BusinessOwnerDashboardStatistic setCoaches(List<CoachNameSessionsView> coaches) {
        this.coaches = coaches;
        return this;
    }
}
