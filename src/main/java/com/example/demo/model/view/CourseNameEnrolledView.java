package com.example.demo.model.view;

public class CourseNameEnrolledView {

    private String name;
    private Integer enrolled;

    public CourseNameEnrolledView() {
    }

    public CourseNameEnrolledView(String name, Integer enrolled) {
        this.name = name;
        this.enrolled = enrolled;
    }

    public String getName() {
        return name;
    }

    public CourseNameEnrolledView setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getEnrolled() {
        return enrolled;
    }

    public CourseNameEnrolledView setEnrolled(Integer enrolled) {
        this.enrolled = enrolled;
        return this;
    }
}
