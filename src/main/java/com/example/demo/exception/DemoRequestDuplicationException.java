package com.example.demo.exception;


public class DemoRequestDuplicationException extends RuntimeException{

    private String email;

    public DemoRequestDuplicationException(String email) {
        super("Demo with email " + email + " has already been requested!");
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}
