package com.example.demo.exception;

import com.example.demo.model.binding.ExceptionDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = InvalidDataException.class)
    public ResponseEntity<?> handleInvalidDataException(InvalidDataException exception){

        ExceptionDto responseDto = new ExceptionDto()
                .setMessage("Invalid data is provided!")
                .setSuccess(Boolean.FALSE);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseDto);

    }

    @ExceptionHandler(value = DemoRequestDuplicationException.class)
    public ResponseEntity<?> handleDemoRequestDuplicationException(DemoRequestDuplicationException exception){

        ExceptionDto responseDto = new ExceptionDto()
                .setMessage(exception.getMessage())
                .setSuccess(Boolean.FALSE);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseDto);

    }
}
