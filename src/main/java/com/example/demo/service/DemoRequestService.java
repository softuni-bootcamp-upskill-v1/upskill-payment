package com.example.demo.service;

import com.example.demo.model.binding.DemoRequestDataBindingModel;

public interface DemoRequestService {
    void saveUserData(DemoRequestDataBindingModel demoRequestDataDto);
}

