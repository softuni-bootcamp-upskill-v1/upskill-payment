package com.example.demo.service.impl;

import com.example.demo.exception.DemoRequestDuplicationException;
import com.example.demo.model.binding.DemoRequestDataBindingModel;
import com.example.demo.model.entity.DemoRequestEntity;
import com.example.demo.repo.DemoRequestRepository;
import com.example.demo.service.DemoRequestService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class DemoRequestServiceImpl implements DemoRequestService {

private final DemoRequestRepository demoRequestRepository;
private final ModelMapper modelMapper;

    public DemoRequestServiceImpl(DemoRequestRepository demoRequestRepository, ModelMapper modelMapper) {
        this.demoRequestRepository = demoRequestRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public void saveUserData(DemoRequestDataBindingModel demoRequestDataDto) {

        if(isExist(demoRequestDataDto)){
            throw new DemoRequestDuplicationException(demoRequestDataDto.getEmail());
        }

        this.demoRequestRepository.insert(this.modelMapper.map(demoRequestDataDto, DemoRequestEntity.class));
    }

    private boolean isExist(DemoRequestDataBindingModel demoRequestDataBindingModel) {

        return this.demoRequestRepository.existsByEmail(demoRequestDataBindingModel.getEmail());
    }
}
