package com.example.demo.service.impl;

import com.example.demo.exception.InvalidDataException;
import com.example.demo.model.entity.BOEntity;
import com.example.demo.model.entity.CourseEntity;
import com.example.demo.model.view.BusinessOwnerDashboardStatistic;
import com.example.demo.model.view.CoachNameSessionsView;
import com.example.demo.model.view.CourseNameEnrolledView;
import com.example.demo.model.view.CourseStatusChange;
import com.example.demo.repo.BusinessOwnerRepository;
import com.example.demo.repo.CourseEntityRepository;
import com.example.demo.service.BOService;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Service
public class BOServiceImpl implements BOService {

    private final BusinessOwnerRepository businessOwnerRepository;
    private final MongoTemplate mongoTemplate;
    private final CourseEntityRepository courseEntityRepository;

    public BOServiceImpl(BusinessOwnerRepository businessOwnerRepository, MongoTemplate mongoTemplate, CourseEntityRepository courseEntityRepository) {
        this.businessOwnerRepository = businessOwnerRepository;
        this.mongoTemplate = mongoTemplate;
        this.courseEntityRepository = courseEntityRepository;
    }

    @Override
    public BusinessOwnerDashboardStatistic getBusinessOwnerCoursesEnrolled(HttpServletRequest request) {

        String email = request.getHeader("X-User-Email");
        BOEntity byEmail = this.businessOwnerRepository.getByEmail(email);

        List<CourseNameEnrolledView> courseEnrolled = new ArrayList<>();

        byEmail.getCourses().forEach(c -> {
            CourseNameEnrolledView course = new CourseNameEnrolledView()
                    .setName(c.getName())
                    .setEnrolled(c.getEnrolled());

            courseEnrolled.add(course);
        });

        List<CoachNameSessionsView> coachSessions = new ArrayList<>();

        byEmail.getCoaches().forEach(c -> {
            CoachNameSessionsView view = new CoachNameSessionsView()
                    .setName(c.getName())
                    .setSessions(c.getBookings().size());

            coachSessions.add(view);
        });

        return new BusinessOwnerDashboardStatistic(courseEnrolled, coachSessions);
    }

    @Override
    public void changeCourseStatus(CourseStatusChange courseStatusChange) {

        Query queryCourseChangeStatus = new Query();
        queryCourseChangeStatus.addCriteria(Criteria.where("email").is(courseStatusChange.getEmail()));

        BOEntity boEntity = this.mongoTemplate.findOne(queryCourseChangeStatus, BOEntity.class);

        if(boEntity == null){
            throw new InvalidDataException();
        }

      boEntity.getCourses().stream().filter(c -> c.getCourseId()
                .equals(courseStatusChange.getCourseId())).findFirst().map(c -> c.setEnrolled(c.getEnrolled() + 1));

        this.businessOwnerRepository.save(boEntity);
    }
}
