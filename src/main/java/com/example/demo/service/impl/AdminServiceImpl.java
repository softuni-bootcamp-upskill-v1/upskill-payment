package com.example.demo.service.impl;

import com.example.demo.model.binding.CoachPaymentModel;
import com.example.demo.model.binding.CoursePaymentModel;
import com.example.demo.model.binding.MonthlyReportView;
import com.example.demo.model.binding.RevenuesView;
import com.example.demo.model.entity.*;
import com.example.demo.model.view.InvoiceModelView;
import com.example.demo.repo.*;
import com.example.demo.service.AdminService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

@Service
public class AdminServiceImpl implements AdminService {
    private final BookEntityRepository bookEntityRepository;
    private final MonthlyReportRepository monthlyReportRepository;
    private final MongoTemplate mongoTemplate;
    private final CoachEntityRepository coachEntityRepository;

    private final BusinessOwnerRepository businessOwnerRepository;

    private final CourseEntityRepository courseEntityRepository;

    public AdminServiceImpl(BookEntityRepository bookEntityRepository, MonthlyReportRepository monthlyReportRepository,
                            MongoTemplate mongoTemplate, CoachEntityRepository coachEntityRepository,
                            BusinessOwnerRepository businessOwnerRepository, CourseEntityRepository courseEntityRepository) {
        this.bookEntityRepository = bookEntityRepository;
        this.monthlyReportRepository = monthlyReportRepository;
        this.mongoTemplate = mongoTemplate;
        this.coachEntityRepository = coachEntityRepository;
        this.businessOwnerRepository = businessOwnerRepository;
        this.courseEntityRepository = courseEntityRepository;
    }

    @Override
    public void addCoachPayment(CoachPaymentModel coachPaymentModel) {
        Query queryForBookCheck = new Query();
        queryForBookCheck.addCriteria(
                Criteria.where("idOfBookedItem")
                        .is(coachPaymentModel.getCoachId())
                        .and("ownerOfBooking")
                        .is(coachPaymentModel.getEmail())
        );

        BookEntity checkInDb = this.mongoTemplate.findOne(queryForBookCheck, BookEntity.class);
        if (checkInDb != null) return;

        LocalDate dateNow = LocalDate.now();
        int month = dateNow.getMonth().getValue();
        int year = dateNow.getYear();

        BookEntity bookEntity = new BookEntity();

        bookEntity.setDateOfBooking(LocalDate.now());
        bookEntity.setPrice(coachPaymentModel.getPrice());
        bookEntity.setMonth(month);
        bookEntity.setIdOfBookedItem(coachPaymentModel.getCoachId());
        bookEntity.setOwnerOfBooking(coachPaymentModel.getEmail());
        bookEntity.setYear(year);

        Query queryForBO = new Query();
        queryForBO.addCriteria(Criteria.where("email").is(coachPaymentModel.getEmail()));
        BOEntity boEntity = this.mongoTemplate.findOne(queryForBO, BOEntity.class);

        if (boEntity == null) {
            BOEntity boPureEntity = new BOEntity();
            boPureEntity.setBusinessOwnerId(coachPaymentModel.getBusinessOwnerId());
            boPureEntity.setEmail(coachPaymentModel.getEmail());
            boPureEntity.setCoaches(new ArrayList<>());
            boPureEntity.setCourses(new ArrayList<>());
            this.businessOwnerRepository.save(boPureEntity);
        }

        BOEntity businessOwner = this.mongoTemplate.findOne(queryForBO, BOEntity.class);

        Query queryForMonthlyReport = new Query();
        queryForMonthlyReport.addCriteria(Criteria.where("month").is(month).and("year").is(year));

        MonthlyReport monthlyReport = this.mongoTemplate.findOne(queryForMonthlyReport, MonthlyReport.class);

        if (monthlyReport == null) {
            MonthlyReport monthlyReportEntity = new MonthlyReport();
            monthlyReportEntity.setMonth(month);
            monthlyReportEntity.setYear(year);
            monthlyReportEntity.setDate(dateNow);
            monthlyReportEntity.setBookings(List.of(bookEntity));

            this.monthlyReportRepository.save(monthlyReportEntity);

        } else {
            monthlyReport.getBookings().add(bookEntity);
            this.monthlyReportRepository.save(monthlyReport);
        }

        Query queryForCoach = new Query();
        queryForCoach.addCriteria(Criteria.where("coachId").is(coachPaymentModel.getCoachId()));

        CoachEntity coachEntity = this.mongoTemplate.findOne(queryForCoach, CoachEntity.class);
        assert businessOwner != null;
        if (coachEntity == null) {
            CoachEntity coachPureEntity = new CoachEntity();

            coachPureEntity.setCoachId(coachPaymentModel.getCoachId());
            coachPureEntity.setCoachPrice(coachPaymentModel.getPrice());
            coachPureEntity.setBookings(List.of(bookEntity));
            coachPureEntity.setName(coachPaymentModel.getName());
            if (businessOwner.getCoaches() == null) {
                businessOwner.setCoaches(List.of(coachPureEntity));
            } else {
                businessOwner.getCoaches().add(coachPureEntity);
            }
            this.coachEntityRepository.save(coachPureEntity);
        } else {
            if (businessOwner.getCoaches() == null) {
                businessOwner.setCoaches(List.of(coachEntity));
            } else {
                businessOwner.getCoaches().add(coachEntity);
            }
            coachEntity.getBookings().add(bookEntity);
            this.coachEntityRepository.save(coachEntity);
        }

        this.businessOwnerRepository.save(businessOwner);
        this.bookEntityRepository.save(bookEntity);
    }

    @Override
    public void addCoursePayment(CoursePaymentModel coursePaymentModel) {


        Query queryForBookCheck = new Query();
        queryForBookCheck.addCriteria(
                Criteria.where("idOfBookedItem")
                        .is(coursePaymentModel.getCourseId())
                        .and("ownerOfBooking")
                        .is(coursePaymentModel.getEmail())
        );

        BookEntity checkInDb = this.mongoTemplate.findOne(queryForBookCheck, BookEntity.class);
        if (checkInDb != null) return;

        LocalDate dateNow = LocalDate.now();
        int month = dateNow.getMonth().getValue();
        int year = dateNow.getYear();

        BookEntity bookEntity = new BookEntity();

        bookEntity.setDateOfBooking(LocalDate.now());
        bookEntity.setPrice(coursePaymentModel.getPrice());
        bookEntity.setMonth(month);
        bookEntity.setIdOfBookedItem(coursePaymentModel.getCourseId());
        bookEntity.setOwnerOfBooking(coursePaymentModel.getEmail());
        bookEntity.setYear(year);

        Query queryForBO = new Query();
        queryForBO.addCriteria(Criteria.where("email").is(coursePaymentModel.getEmail()));
        BOEntity boEntity = this.mongoTemplate.findOne(queryForBO, BOEntity.class);

        if (boEntity == null) {
            BOEntity boPureEntity = new BOEntity();
            boPureEntity.setBusinessOwnerId(coursePaymentModel.getBusinessOwnerId());
            boPureEntity.setEmail(coursePaymentModel.getEmail());
            boPureEntity.setCoaches(new ArrayList<>());
            boPureEntity.setCourses(new ArrayList<>());
            this.businessOwnerRepository.save(boPureEntity);
        }


        BOEntity businessOwner = this.mongoTemplate.findOne(queryForBO, BOEntity.class);

        Query queryForMonthlyReport = new Query();
        queryForMonthlyReport.addCriteria(Criteria.where("month").is(month).and("year").is(year));

        MonthlyReport monthlyReport = this.mongoTemplate.findOne(queryForMonthlyReport, MonthlyReport.class);

        if (monthlyReport == null) {
            MonthlyReport monthlyReportEntity = new MonthlyReport();
            monthlyReportEntity.setMonth(month);
            monthlyReportEntity.setYear(year);
            monthlyReportEntity.setDate(dateNow);
            monthlyReportEntity.setBookings(List.of(bookEntity));

            this.monthlyReportRepository.save(monthlyReportEntity);

        } else {
            monthlyReport.getBookings().add(bookEntity);
            this.monthlyReportRepository.save(monthlyReport);
        }

        Query queryForCoach = new Query();
        queryForCoach.addCriteria(Criteria.where("courseId").is(coursePaymentModel.getCourseId()));

        CourseEntity courseEntity = this.mongoTemplate.findOne(queryForCoach, CourseEntity.class);
        assert businessOwner != null;

        if (courseEntity == null) {
            CourseEntity coursePureEntity = new CourseEntity();

            coursePureEntity.setCourseId(coursePaymentModel.getCourseId());
            coursePureEntity.setCoursePrice(coursePaymentModel.getPrice());
            coursePureEntity.setBookings(List.of(bookEntity));
            coursePureEntity.setEnrolled(coursePaymentModel.getEnrolled());
            coursePureEntity.setName(coursePaymentModel.getName());

            if (businessOwner.getCourses() == null) {
                businessOwner.setCourses(List.of(coursePureEntity));
            } else {
                businessOwner.getCourses().add(coursePureEntity);
            }
            this.courseEntityRepository.save(coursePureEntity);
        } else {
            if (businessOwner.getCourses() == null) {
                businessOwner.setCourses(List.of(courseEntity));
            } else {
                businessOwner.getCourses().add(courseEntity);
            }
            courseEntity.getBookings().add(bookEntity);
            this.courseEntityRepository.save(courseEntity);
        }

        this.businessOwnerRepository.save(businessOwner);
        this.bookEntityRepository.save(bookEntity);
    }

    @Override
    public MonthlyReportView getMonthlyReports() {
        MonthlyReportView monthlyReportView = new MonthlyReportView();

        List<String> months = new ArrayList<>();
        List<Integer> monthNumbers = new ArrayList<>();
        List<BigDecimal> revenues = new ArrayList<>();

        LocalDate dateNow = LocalDate.now();
        int year = dateNow.getYear();
        int month = dateNow.getMonth().getValue();
        Optional<MonthlyReport> monthlyReport = this.monthlyReportRepository.findByYearAndMonth(year, month);
        if (monthlyReport.isPresent()) getData(months, monthNumbers, revenues, monthlyReport);
        month--;
        for (int i = 0; i < 6; i++) {
            if (month == 0) {
                month = 12;
                year = year - 1;
                Optional<MonthlyReport> pureEntity = this.monthlyReportRepository.findByYearAndMonth(year, month);
                if (pureEntity.isPresent()) getData(months, monthNumbers, revenues, pureEntity);

            } else {
                Optional<MonthlyReport> pureEntity = this.monthlyReportRepository.findByYearAndMonth(year, month);
                if (pureEntity.isPresent()) getData(months, monthNumbers, revenues, pureEntity);
            }
            month--;
        }
        Collections.reverse(months);
        Collections.reverse(revenues);
        Collections.reverse(monthNumbers);

        monthlyReportView.setMonthNumbers(monthNumbers);
        monthlyReportView.setMonths(months);
        monthlyReportView.setRevenues(revenues);

        return monthlyReportView;
    }

    @Override
    public List<RevenuesView> getRevenues(int page, int size) {
        page--;

        Pageable coursePageable = PageRequest.of(page, size);
        this.coachEntityRepository.findAll(coursePageable);

        Pageable coachPageable = PageRequest.of(page, size);
        List<CoachEntity> coaches = this.coachEntityRepository.findAll(coachPageable)
                .getContent()
                .stream().toList();

        List<CourseEntity> courses = this.courseEntityRepository.findAll(coursePageable)
                .getContent()
                .stream().toList();

        List<RevenuesView> revenuesViews = new ArrayList<>();

        coaches.forEach(c -> {
            BigDecimal price = BigDecimal.ZERO;
            RevenuesView revenuesView = new RevenuesView();
            revenuesView.setName("Coach: " + c.getName());
            List<BookEntity> bookings = c.getBookings();

            for (BookEntity bookEntity : bookings) {
                price = price.add(bookEntity.getPrice());
            }
            revenuesView.setMoney(price);
            revenuesViews.add(revenuesView);
        });

        courses.forEach(c -> {
            BigDecimal price = BigDecimal.ZERO;
            RevenuesView revenuesView = new RevenuesView();
            revenuesView.setName("Course: " + c.getName());
            List<BookEntity> bookings = c.getBookings();

            for (BookEntity bookEntity : bookings) {
                price = price.add(bookEntity.getPrice());
            }
            revenuesView.setMoney(price);
            revenuesViews.add(revenuesView);
        });


        return revenuesViews;

    }

    @Override
    public List<InvoiceModelView> findInvoiceModelsByMonth(Integer month, Integer year, String email) {

        List<InvoiceModelView> invoices = new ArrayList<>();

        BOEntity bo_entity = this.businessOwnerRepository.getByEmail(email);

        bo_entity.getCourses().stream().forEach(c -> {
            c.getBookings().stream().forEach(b -> {
                if (b.getMonth().equals(month) && b.getYear().equals(year) && b.getOwnerOfBooking().equals(email)) {
                    InvoiceModelView invoice = new InvoiceModelView();
                    invoice.setDate(b.getDateOfBooking());
                    invoice.setPrice(b.getPrice());
                    invoice.setModelName("Course: " + c.getName());
                    invoices.add(invoice);
                }
            });
        });

        bo_entity.getCoaches().forEach(c -> {
            c.getBookings().forEach(b -> {
                if (b.getMonth().equals(month) && b.getYear().equals(year) && b.getOwnerOfBooking().equals(email)) {
                    InvoiceModelView invoice = new InvoiceModelView();
                    invoice.setDate(b.getDateOfBooking());
                    invoice.setPrice(b.getPrice());
                    invoice.setModelName("Coach: " + c.getName());
                    invoices.add(invoice);
                }
            });
        });
        return invoices;
    }

    private void getData(List<String> months, List<Integer> monthNumbers, List<BigDecimal> revenues, Optional<MonthlyReport> monthlyReport) {
        BigDecimal sum = BigDecimal.ZERO;

        for (BookEntity a : monthlyReport.get().getBookings()) {
            sum = sum.add(a.getPrice());
        }

        months.add(getMonth(monthlyReport.get().getMonth()));
        revenues.add(sum);
        monthNumbers.add(monthlyReport.get().getMonth());
    }

    private String getMonth(Integer month) {
        return switch (month) {
            case 1 -> "January";
            case 2 -> "February";
            case 3 -> "Mart";
            case 4 -> "April";
            case 5 -> "May";
            case 6 -> "June";
            case 7 -> "July";
            case 8 -> "August";
            case 9 -> "September";
            case 10 -> "October";
            case 11 -> "November";
            default -> "December";
        };
    }
}
