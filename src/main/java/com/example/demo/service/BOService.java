package com.example.demo.service;

import com.example.demo.model.view.BusinessOwnerDashboardStatistic;
import com.example.demo.model.view.CourseNameEnrolledView;
import com.example.demo.model.view.CourseStatusChange;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface BOService {
    BusinessOwnerDashboardStatistic getBusinessOwnerCoursesEnrolled(HttpServletRequest request);

    void changeCourseStatus(CourseStatusChange courseStatusChange);
}


