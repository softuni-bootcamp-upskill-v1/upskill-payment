package com.example.demo.service;

import com.example.demo.model.binding.CoachPaymentModel;
import com.example.demo.model.binding.CoursePaymentModel;
import com.example.demo.model.binding.MonthlyReportView;
import com.example.demo.model.binding.RevenuesView;
import com.example.demo.model.view.InvoiceModelView;

import java.util.List;

public interface AdminService {
    void addCoachPayment(CoachPaymentModel coachPaymentModel);

    void addCoursePayment(CoursePaymentModel coursePaymentModel);

    MonthlyReportView getMonthlyReports();

    List<RevenuesView> getRevenues(int page, int size);

    List<InvoiceModelView> findInvoiceModelsByMonth(Integer month, Integer year, String email);
}
