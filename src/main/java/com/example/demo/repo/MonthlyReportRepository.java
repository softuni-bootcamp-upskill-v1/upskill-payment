package com.example.demo.repo;

import com.example.demo.model.entity.MonthlyReport;
import org.springframework.data.mongodb.core.MongoAdminOperations;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MonthlyReportRepository extends MongoRepository<MonthlyReport, String> {

//    @Query("{year:  ?0 , month: ?1}")
    Optional<MonthlyReport> findByYearAndMonth(int year, int month);
}
