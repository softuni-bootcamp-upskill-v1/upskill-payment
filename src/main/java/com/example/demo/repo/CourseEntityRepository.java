package com.example.demo.repo;

import com.example.demo.model.entity.CourseEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseEntityRepository extends MongoRepository<CourseEntity, String> {

    Page<CourseEntity> findAll(Pageable pageable);
}
