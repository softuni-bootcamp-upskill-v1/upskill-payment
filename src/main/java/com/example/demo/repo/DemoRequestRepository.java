package com.example.demo.repo;

import com.example.demo.model.entity.DemoRequestEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DemoRequestRepository extends MongoRepository<DemoRequestEntity, String> {

    boolean existsByEmail(String email);
}
