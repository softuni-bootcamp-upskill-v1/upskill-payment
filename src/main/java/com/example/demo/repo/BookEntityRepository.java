package com.example.demo.repo;

import com.example.demo.model.entity.BookEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookEntityRepository  extends MongoRepository<BookEntity,String> {
}
