package com.example.demo.repo;

import com.example.demo.model.entity.BOEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BORepository extends MongoRepository<BOEntity, String> {

}
