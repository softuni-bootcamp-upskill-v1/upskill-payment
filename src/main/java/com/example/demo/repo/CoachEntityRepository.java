package com.example.demo.repo;

import com.example.demo.model.entity.CoachEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoachEntityRepository extends MongoRepository<CoachEntity, String> {

    Page<CoachEntity> findAll(Pageable pageable);
}
