package com.example.demo.repo;

import com.example.demo.model.entity.BOEntity;
import com.example.demo.model.entity.CoachEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BusinessOwnerRepository extends MongoRepository<BOEntity,String> {

    BOEntity getByEmail(String email);

}
