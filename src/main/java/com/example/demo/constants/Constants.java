package com.example.demo.constants;

public class Constants {
    public static final String COACH_PAYMENT_MODEL = "coach_payment_model";
    public static final String COURSE_PAYMENT_MODEL = "course_payment_model";

    public static final String COURSE_STATUS_CHANGE = "course_status_change";
}
