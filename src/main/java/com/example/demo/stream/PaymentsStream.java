package com.example.demo.stream;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

import static com.example.demo.constants.Constants.*;

public interface PaymentsStream {


    @Input(COACH_PAYMENT_MODEL)
    SubscribableChannel getCoachPaymentModel();

    @Input(COURSE_PAYMENT_MODEL)
    SubscribableChannel getCoursePaymentModel();

    @Input(COURSE_STATUS_CHANGE)
    SubscribableChannel getCourseStatusChange();

}
