package com.example.demo.controller;

import com.example.demo.exception.InvalidDataException;
import com.example.demo.model.binding.DemoRequestDataBindingModel;
import com.example.demo.service.DemoRequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/payment")
public class DemoRequestController {

    private final DemoRequestService demoRequestService;

    public DemoRequestController(DemoRequestService demoRequestService) {
        this.demoRequestService = demoRequestService;
    }

    @PostMapping("/demo/request")
    public ResponseEntity<?> demoRequestData(@Valid @RequestBody DemoRequestDataBindingModel demoRequestDataDto, BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            throw new InvalidDataException();
        }

       this.demoRequestService.saveUserData(demoRequestDataDto);

        return ResponseEntity.ok().build();
    }
}
