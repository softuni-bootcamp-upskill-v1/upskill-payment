package com.example.demo.controller;

import com.example.demo.model.binding.CoachPaymentModel;
import com.example.demo.model.binding.MonthlyReportView;
import com.example.demo.model.view.BusinessOwnerDashboardStatistic;
import com.example.demo.model.view.CourseNameEnrolledView;
import com.example.demo.model.view.CourseStatusChange;
import com.example.demo.service.BOService;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static com.example.demo.constants.Constants.COACH_PAYMENT_MODEL;
import static com.example.demo.constants.Constants.COURSE_STATUS_CHANGE;

@RestController
public class BusinessOwnerController {

    private final BOService boService;

    public BusinessOwnerController(BOService boService) {
        this.boService = boService;
    }

    @GetMapping("/bo-statistic")
    public ResponseEntity<?> getBusinessOwnerCoursesEnrolled(HttpServletRequest request) {
        BusinessOwnerDashboardStatistic courseNameEnrolledView = this.boService.getBusinessOwnerCoursesEnrolled(request);

        return ResponseEntity.ok(courseNameEnrolledView);
    }

    @StreamListener(COURSE_STATUS_CHANGE)
    public void handleCoachPaymentModel(@Payload CourseStatusChange courseStatusChange) {
        this.boService.changeCourseStatus(courseStatusChange);
    }
}
