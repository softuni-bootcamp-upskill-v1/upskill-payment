package com.example.demo.controller;

import com.example.demo.model.binding.CoachPaymentModel;
import com.example.demo.model.binding.CoursePaymentModel;
import com.example.demo.model.binding.MonthlyReportView;
import com.example.demo.model.binding.RevenuesView;
import com.example.demo.model.view.InvoiceModelView;
import com.example.demo.service.AdminService;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static com.example.demo.constants.Constants.COACH_PAYMENT_MODEL;
import static com.example.demo.constants.Constants.COURSE_PAYMENT_MODEL;

@RestController
public class AdminController {

    private final AdminService adminService;

    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @StreamListener(COACH_PAYMENT_MODEL)
    public void handleCoachPaymentModel(@Payload CoachPaymentModel coachPaymentModel) {
        this.adminService.addCoachPayment(coachPaymentModel);
    }


    @StreamListener(COURSE_PAYMENT_MODEL)
    public void handleCoursePaymentModel(@Payload CoursePaymentModel coursePaymentModel) {
        this.adminService.addCoursePayment(coursePaymentModel);
    }

    @GetMapping("/monthly-report")
    public ResponseEntity<?> getMonthlyReport() {
        MonthlyReportView monthlyReports = this.adminService.getMonthlyReports();

        return ResponseEntity.ok(monthlyReports);
    }

    @GetMapping("/revenues")
    public ResponseEntity<?> getRevenues(@RequestParam(defaultValue = "0") int page,
                                         @RequestParam(defaultValue = "2") int size) {

        List<RevenuesView> revenues = this.adminService.getRevenues(page, size);

        return ResponseEntity.ok(revenues);
    }

    @GetMapping("/bo-invoices")
    public ResponseEntity<?> getInvoiceByMonth(@RequestParam(value = "month") Integer month,
                                               @RequestParam(value = "year") Integer year,
                                               HttpServletRequest request) {
        String email = request.getHeader("X-User-Email");
        List<InvoiceModelView> list = this.adminService.findInvoiceModelsByMonth(month, year, email);

        return ResponseEntity.ok(list);
    }
}
